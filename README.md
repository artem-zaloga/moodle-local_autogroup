# local_autogroup

This is a plugin that automatically assigns random groups to users that self enrol into a course. The available groups are set in the corresponding course custom field.
