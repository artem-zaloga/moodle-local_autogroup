<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Autogroup install hook.
 *
 * @package    local_autogroup
 * @copyright  2019 Artem Zaloga
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use core_customfield\handler;
use customfield_checkbox\field_controller;
use core_customfield\api;

function xmldb_local_autogroup_install() {

    // Create the custom field that governs groups
    $handler = handler::get_handler('core_course', 'course');

    $catid = $handler->create_category('Autogroup');
    $record = $data = new \stdClass;

    $record->type = 'text';
    $record->categoryid = $catid;
    $data->type = 'text';
    $data->name = 'Self-enrol auto-groups';
    $data->shortname = 'autogroups';
    $data->description = "<p>Comma separated list of groups to auto-group self-enrolled users into; if empty, no auto-grouping will be done.<br><em>These groups must already exist in this course.</em></p>";
    $data->descriptionformat = 1;
    $data->configdata = [
        'required' => '0',
        'uniquevalues' => '0',
        'defaultvalue' => '',
        'displaysize' => 50,
        'maxlength' => 1333,
        'ispassword' => '0',
        'link' => '',
        'locked' => '0',
        'visibility' => '2'
    ];
    $controller = field_controller::create(0, $record);
    api::save_field_configuration($controller, $data);

}
