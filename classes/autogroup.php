<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contains class local_autogroup\autogroup
 *
 * @package    local_autogroup
 * @copyright  2019 Artem Zaloga
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_autogroup;

use core_course_list_element;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/group/lib.php');


class autogroup
{
    public static function assign_group($event)
    {
        global $DB;

        $data = $event->get_data();
        $courseid = $data['courseid'];
        $userid = $data['relateduserid'];
        $enroltype = $data['other']['enrol'];

        // continue only if enroltype is 'self'
        if ($enroltype == 'self') {

            // get this course custom field 'autogroups' value
            $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
            $course = new core_course_list_element($course);
            $customfields = $course->get_custom_fields();
            $autogroups = '';
            foreach ($customfields as $data) {
                if ($data->get_field()->get('shortname') == 'autogroups') {
                    $autogroups = $data->export_value();
                    $autogroups = explode(",", $autogroups);
                    $autogroups = array_map('trim', $autogroups);
                }
            }

            // continue only if autogroups is not empty for this course
            if (!empty($autogroups)) {

                // get course groups and store their names, but only if they are included in the autogroups custom field array
                $groupobjects = groups_get_course_data($courseid)->groups;
                $groups = array();
                foreach ($groupobjects as $group) {
                    if (in_array($group->name, $autogroups)) {
                          array_push($groups, $group->name);
                    }
                }

                // continue only if there are groups in this course
                if (!empty($groups)) {
                    // count which group to add user to
                    $enroleduserscount = count_enrolled_users($course->get_context()) - 1;
                    $groupvalue = $enroleduserscount % count($groups);
                    $groupname = $groups[$groupvalue];

                    // get groupid and userid and add user to group
                    $groupid = groups_get_group_by_name($courseid, $groupname);
                    groups_add_member($groupid, $userid);
                }
            }
        }
    }
}
